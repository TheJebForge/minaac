#include "minaac.h"

int myCustomFunc(minaac::mStack* in){
    int a = in->getNext();
    return a+5;
}

int debug(minaac::mStack* in){
    std::cout << in->getNext() << "\n";
}

std::string code = R"(
buf 0x0 in
buf 0x1 out


put {{{ Minaac 1.0 Live Interpreter }}} 0x1
mov 10 0x1
put {{{ Type 'ext' to exit }}} 0x1
mov 10 0x1
mov 13 0x1

lbl linestart
mov 25 0x1
mov 10 0x1
mov 62 0x1
mov 32 0x1
mov 13 0x1
mov 25 0x0
mov 7 0x0

buf 0x11 buffer
buf 0x12 buffer
mpt 0x0 0x11
mpt 0x11 0x12

lbl parser
mov 0x12 0x13
jnq 0x13 101 run
mov 0x12 0x13
jnq 0x13 120 run
mov 0x12 0x13
jnq 0x13 116 run
jmp end

lbl run
exc 0x11
jmp linestart

lbl end
)";

int main(){
    minaac::mState* a = new minaac::mState;
    a->addFunction("custom",1,&myCustomFunc);
    a->addFunction("debug",1,&debug);
    a->executeCode(code);
}

