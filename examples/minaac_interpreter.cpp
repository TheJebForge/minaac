#include "../minaac.h"

class DebugCell : public minaac::MemoryCell { // Making a new custom buffer
    void setData(int stuff){ // Is used when commands like mov, add, sub... is used towards the buffer
        cout << stuff << "\n";
    }

    int getData(){ // Is used when commands gather any data from the buffer, like mov <buffer address> <some cell/buffer address> and etc...
	return 0;
    }

    void acceptPut(string in){ // Is used for put and mpt commands
	cout << in << "\n";
    }

    string returnPut(){ //Is used for mpt, exc commands
	return "I have nothing to output";
    }
};

minaac::MemoryCell* debugCellInit(){ // Required for being used in buf command, to init buffer from minaac, do "buf 0x1 <name of the buffer later on>"
    return new DebugCell;
}

int myCustomFunc(minaac::mStack* in){ // Custom functions, must return int and accept mStack*
    int a = in->getNext();
    return a+5;
}

int debug(minaac::mStack* in){
    cout << in->getNext() << "\n";
}

int cuddle(minaac::mStack* in){
    cout << "*cuddles~*\n";
    return 0;
}

int snuggle(minaac::mStack* in){
    cout << "*snuggles tight~*\n";
    return 0;
}

string code = R"(
buf 0x0 in
buf 0x1 out


put {{{ Minaac 1.0 Live Interpreter }}} 0x1
mov 10 0x1
put {{{ Type 'ext' to exit }}} 0x1
mov 10 0x1
mov 13 0x1

lbl linestart
mov 25 0x1
mov 10 0x1
mov 62 0x1
mov 32 0x1
mov 13 0x1
mov 25 0x0
mov 7 0x0

buf 0x11 buffer
buf 0x12 buffer
mpt 0x0 0x11
mpt 0x11 0x12

lbl parser
mov 0x12 0x13
jnq 0x13 101 run
mov 0x12 0x13
jnq 0x13 120 run
mov 0x12 0x13
jnq 0x13 116 run
jmp end

lbl run
exc 0x11
jmp linestart

lbl end
)";

int main(){
    minaac::mState* a = new minaac::mState; // Way to open a new minaac state
    a->addBuffer("debug", &debugCellInit); // Adds buffer to be used with buf command, first argument is name of buffer that will be used in buf command, second is pointer to buffer init function
    a->addFunction("custom",1,&myCustomFunc); // Adds custom function to be used in exf command (exf <function name> <args> <output cell/nul>), first argument is name of function, second is amount of arguments, third is pointer to function
    a->addFunction("debug",1,&debug);
    a->addFunction("cdl",0,&cuddle);
    a->addFunction("sngl",0,&snuggle);
    // a->setCellPointer(MemoryCell* cell, int address) - can be used to directly set a cell as a buffer
    // MemoryCell* a->getCellPointer(int address) - returns pointer to a memory cell to be interacted with directly
    a->executeCode(code); // Executes a string as minaac code
    // a->executeFile(string filename) - can be used to execute a file if you don't want to mess with ifstream yourself
}
