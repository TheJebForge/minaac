#ifndef MINAAC_H_INCLUDED
#define MINAAC_H_INCLUDED

#include <iostream>
#include <sstream>
#include <bits/stdc++.h>
#include <vector>
#include <fstream>
#include <streambuf>
#include <string>



uint64_t constexpr mix(char m, uint64_t s)
{
	return ((s<<7) + ~(s>>3)) + ~m;
}

uint64_t constexpr h(const char * m)
{
	return (*m) ? mix(*m,h(m+1)) : 0;
}

namespace minaac
{
    using namespace std;

	class MemoryCell
	{
	private:
		int data=0;
	public:
		virtual int getData()
		{
			return data;
		}
		virtual void setData(int newdata)
		{
			data = newdata;
		}

		virtual void acceptPut(string in)
		{
			if(in.length()>0)
			{
				data = (int)in.substr(0,1).c_str()[0];
			}
		}

		virtual string returnPut()
		{
			string a;
			a += (char)data;
			return a;
		}

	};

	class DebugCell : public MemoryCell {
		void setData(int stuff){
			cout << stuff << "\n";
		}
	};

	class InputCell : public MemoryCell
	{
		string buffer;
	public:
		int getData()
		{
			if(buffer.length()>0)
			{
				char out = buffer[0];
				buffer.erase(0,1);
				return out;
			}
			else
			{
				return 0;
			}
		}
		void setData(int dat)
		{
			if(dat==7)
			{
				char c[1000];
				cin.getline(c,1000);
				buffer += c;
			}
			else if(dat==25)
			{
				buffer = "";
			}
		}

		string returnPut()
		{
			return buffer;
		}
	};

	class OutputCell : public MemoryCell
	{
		string buffer;
		int pos = 0;
	public:
		int getData()
		{
			return -1;
		}

		void setData(int dat)
		{
			char c = dat;
			if(c==13)
				cout << buffer;
			else if(c==2)
				pos = 0;
			else if(c==3)
				pos = buffer.length();
			else if(c==21)
			{
				if(pos>0)
					pos--;
			}
			else if(c==22)
			{
				if(pos<buffer.length()-1)
					pos++;
			}
			else if(c==8)
			{
				if(pos>0&&pos<=buffer.length())
					buffer.erase(pos-1,1);
			}
			else if(c==25)
			{
				pos = 0;
				buffer.clear();
			}
			else
			{
				if(c==10)
					c = '\n';
				if(pos>=buffer.length())
					buffer += c;
				else
					buffer.insert(pos,1,c);
				pos++;
			}
		}

		void acceptPut(string in)
		{
			buffer += in;
			pos = buffer.length();
		}

		string returnPut()
		{
			return buffer;
		}
	};

	class FileBuffer : public MemoryCell
	{
		fstream buffer;
		string filename;
		int len=0;
		int mode=0;
		int status=0;
	public:
		int getData()
		{
			if(mode==1)
			{
				char out=0;
				if(!buffer.eof())
					buffer.read(&out,1);
				return out;
			}
			else
			{
				return 0;
			}
		}

		void setData(int dat)
		{
			if(!buffer.is_open())
			{
				if(status==0)
				{
					if(dat!=13)
						filename += (char)dat;
					else
						status = 1;
				}
				else if(status==1)
				{
					mode = dat;
					status = 0;
					switch(mode)
					{
					case 1:
						buffer.open(filename.c_str(), fstream::in);
						break;
					case 2:
						buffer.open(filename.c_str(), fstream::out);
						break;
					case 3:
						buffer.open(filename.c_str(), fstream::out | fstream::app);
						break;
					}
				}
			}
			else
			{
				if(buffer.good())
				{
					char c = dat;
					if(c==13)
					{
						if(mode!=1)
							buffer.flush();
					}
					else if(c==2)
					{
						if(mode==1)
							buffer.seekg(0,buffer.beg);
						else
							buffer.seekp(0,buffer.beg);
					}
					else if(c==3)
					{
						if(mode==1)
							buffer.seekg(0,buffer.end);
						else
							buffer.seekp(0,buffer.end);
					}
					else if(c==21)
					{
						if(mode==1)
							buffer.seekg(-1,buffer.cur);
						else
							buffer.seekp(-1,buffer.cur);
					}
					else if(c==22)
					{
						if(mode==1)
							buffer.seekg(1,buffer.cur);
						else
							buffer.seekp(1,buffer.cur);
					}
					else if(c==27)
					{
						buffer.close();
					}
					else
					{
						if(c==10)
							c = '\n';
						if(mode!=1)
						{
							buffer.write(&c,1);
						}
					}
				}
				else
				{
					buffer.close();
				}
			}
		}

		void acceptPut(string in)
		{
			if(buffer.is_open())
			{
				if(mode!=1)
				{
					buffer.write(in.c_str(),in.length());
				}
			}
			else
			{
				if(status==0)
				{
					filename = in;
					status = 1;
				}
			}
		}

		string returnPut()
		{
			if(mode==1)
			{
				if(buffer.is_open())
				{
					string s;
					buffer.seekg(0,buffer.end);
					s.reserve(buffer.tellg());
					buffer.seekg(0,buffer.beg);
					s.assign(istreambuf_iterator<char>(buffer),istreambuf_iterator<char>());
					return s;
				}
			}
			else
			{
				return "";
			}
		}
	};

	class BufCell : public MemoryCell
	{
		string buffer;
	public:
		int pos = 0;

		int getData()
		{
			if(buffer.length()>0)
			{
				char out = buffer[0];
				buffer.erase(0,1);
				return out;
			}
			else
			{
				return 0;
			}
		}

		void setData(int dat)
		{
			char c = dat;
			if(c==2)
				pos = 0;
			else if(c==3)
				pos = buffer.length();
			else if(c==21)
			{
				if(pos>0)
					pos--;
			}
			else if(c==22)
			{
				if(pos<buffer.length()-1)
					pos++;
			}
			else if(c==8)
			{
				if(pos>0&&pos<=buffer.length())
					buffer.erase(pos-1,1);
			}
			else if(c==25)
			{
				pos = 0;
				buffer.clear();
			}
			else
			{
				if(c==10)
					c = '\n';
				if(pos>=buffer.length())
					buffer += c;
				else
					buffer.insert(pos,1,c);
				pos++;
			}
		}

		void acceptPut(string in)
		{
			buffer += in;
			pos = buffer.length();
		}

		string returnPut()
		{
			return buffer;
		}
	};

	struct mStack
	{
		int index=0;
		vector<int> arra;
		int getNext()
		{
			if(index>=arra.size())
				return 0;
			return arra[index++];
		}
	};

	class mState
	{
		struct alias{
			string name;
			string replacement;

			alias(string name, string replacement){
				this->name = name;
				this->replacement = replacement;
			}
		};

		vector<alias> aliases;



		string spacer(string in, int len)
		{
			while(in.size()<len)
			{
				in = " " + in;
			}
			return in;
		}

		string itos(int i)
		{
			stringstream str;
			str << i;
			string out;
			str >> out;
			return out;
		}

		int hstoi(string i)
		{
			stringstream str;
			str << hex << i;
			int out;
			str >> out;
			return out;
		}

		int nstoi(string i)
		{
			stringstream str;
			str << i;
			int out;
			str >> out;
			return out;
		}

		string checkInAliases(string what){
			for(int i=0;i<aliases.size();i++){
				if(aliases[i].name == what){
					return aliases[i].replacement;
				}
			}
			return "";
		}


		int exind = 0;
		string exword = "";

		struct mFunc
		{
			int argcount=0;
			string name;
			int (* func)(mStack*);
		};

		struct mBuffer
		{
			string name;
			MemoryCell* (* func)();
		};

		vector<mFunc*> functions;
		vector<mBuffer*> buffers;

		MemoryCell* cells[10000000];

		struct arr
		{
			int index=0;
			vector<string> arra;
			string getNext()
			{
				if(index>=arra.size())
					return "";
				return arra[index++];
			}
		};

		struct procedure {
			string code = "";
			string name = "";
		};

		vector<procedure> procedures;

		string checkInProcedures(string what){
			for(int i=0;i<procedures.size();i++){
				if(procedures[i].name == what){
					return procedures[i].code;
				}
			}
			return "";
		}

		struct lbl
		{
			int index;
			string label;
		};

		bool checkArg(string thing, arr* in)
		{
			if(checkInAliases(thing).length()>0) return true;

			if(thing.length()<3)
			{
				regex re("[^\\d]+");
				smatch match;
				regex_search(thing, match, re);
				if(match.size()>0)
				{
					cout << "Wrong argument '" << thing << "' is found at " << in->index << "\n";
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				regex re("^0x[0-9abcdef*i]+$");
				smatch match;
				regex_search(thing, match, re);
				if(match.size()==0)
				{
					regex re1("[^\\d]+");
					smatch match1;
					regex_search(thing, match1, re1);
					if(match1.size()>0)
					{
						cout << "Wrong argument '" << thing << "' is found at " << in->index << "\n";
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					regex re1("[*i]");
					smatch match1;
					regex_search(thing, match1, re1);
					if(match1.size()>0)
					{
						regex re2("[*i]$");
						smatch match2;
						regex_search(thing, match2, re2);
						if(match2.size()==0)
						{
							cout << "Wrong placement of * or i '" << thing << "' is found at " << in->index << "\n";
							return false;
						}
						else
						{
							return true;
						}
					}
					else
					{
						return true;
					}
				}
			}
		}

		int getBufferOrNumber(string thing)
		{
			string repl = checkInAliases(thing);
			if(repl.length()>0) thing = repl;

			if(thing.substr(1,1)=="x")
			{
				if(thing.substr(thing.length()-1,1)!="i")
				{
					if(thing.substr(thing.length()-1,1)!="*")
					{
						int cell = hstoi(thing);
						if(cells[cell] != NULL)
							return cells[cell]->getData();
						else
							return 0;
					}
					else
					{
						int cell = getBuffer(thing.substr(0,thing.length()));
						if(cells[cell]!=NULL)
							return cells[cell]->getData();
						else
							return 0;
					}
				}
				else
				{
					if(thing.substr(thing.length()-1,1)!="*")
					{
						return hstoi(thing.substr(0,thing.length()-1));
					}
				}
			}
			else
			{
				return nstoi(thing);
			}
		}

		int getBuffer(string thing)
		{
			string repl = checkInAliases(thing);
			if(repl.length()>0) thing = repl;

			if(thing.substr(1,1)=="x")
			{
				if(thing.substr(thing.length()-1,1)!="i")
				{
					if(thing.substr(thing.length()-1,1)!="*")
					{
						int cell = hstoi(thing);
						return cell;
					}
					else
					{
						int cell = getBuffer(thing.substr(0,thing.length()-1));
						if(cells[cell]!=NULL)
							return cells[cell]->getData();
						else
							return 0;
					}
				}
				else
				{
					return hstoi(thing.substr(0,thing.length()-1));
				}
			}
			else
			{
				return nstoi(thing);
			}
		}

		void setBuffer(int cell, int data)
		{
			if(cell>=0)
			{
				if(cells[cell] != NULL)
					cells[cell]->setData(data);
				else
				{
					cells[cell] = new MemoryCell();
					cells[cell]->setData(data);
				}
			}
		}

		void mov(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			setBuffer(getBuffer(ar2),getBufferOrNumber(ar1));
		}

		void buf(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int cell = getBuffer(ar1);
			if(cell>=0)
			{
				if(cells[cell] != NULL)
					delete(cells[cell]);
				string ar2 = in->getNext();
				switch(h(ar2.c_str()))
				{
				case h("out"):
					cells[cell] = new OutputCell;
					break;
				case h("in"):
					cells[cell] = new InputCell;
					break;
				case h("file"):
					cells[cell] = new FileBuffer;
					break;
				case h("buffer"):
					cells[cell] = new BufCell;
					break;
				case h("cell"):
					cells[cell] = new MemoryCell;
					break;
				case h("debug"):
					cells[cell] = new DebugCell;
					break;
				case h("gbuffer"):
					if(globalBuffer==NULL)
						globalBuffer = new BufCell;
					cells[cell] = globalBuffer;
					break;
				default:
					for(int h=0; h<buffers.size(); h++)
					{
						if(buffers[h]->name == ar2)
						{
							cells[cell] = buffers[h]->func();
							return;
						}
					}
					cerr << "Buffer '" << ar2 << "' is not found at " << exind << " (" << exword << ")\n";
					if(cells[cell]!=NULL)
						cells[cell] = new MemoryCell;
					break;
				}
			}
		}

		vector<lbl> labels;
		int lastpos;

		void jmp(arr* in)
		{
			string lab = in->getNext();
			for(int i=0; i<labels.size(); i++)
			{
				if(labels[i].label==lab)
				{
					lastpos = in->index;
					in->index = labels[i].index;
					return;
				}
			}
			cerr << "Label '" << lab << "' is not found at " << exind << " (" << exword << ")\n";
		}

		void bjp(arr* in)
		{
			in->index = lastpos;
		}

		void jgt(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int a = getBufferOrNumber(ar1);
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			int b = getBufferOrNumber(ar2);
			if(a>b)
				jmp(in);
		}

		void jlt(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int a = getBufferOrNumber(ar1);
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			int b = getBufferOrNumber(ar2);
			if(a<b)
				jmp(in);
		}

		void jeq(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int a = getBufferOrNumber(ar1);
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			int b = getBufferOrNumber(ar2);
			if(a==b)
				jmp(in);
		}

		void jnq(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int a = getBufferOrNumber(ar1);
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			int b = getBufferOrNumber(ar2);
			if(a!=b)
				jmp(in);
		}

		void add(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			setBuffer(getBuffer(ar2),getBufferOrNumber(ar2)+getBufferOrNumber(ar1));
		}

		void sub(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			setBuffer(getBuffer(ar2),getBufferOrNumber(ar2)-getBufferOrNumber(ar1));
		}

		void mul(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			setBuffer(getBuffer(ar2),getBufferOrNumber(ar2)*getBufferOrNumber(ar1));
		}

		void div(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			setBuffer(getBuffer(ar2),getBufferOrNumber(ar2)/getBufferOrNumber(ar1));
		}

		void del(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int cell = getBuffer(ar1);
			if(cell>=0)
			{
				if(cells[cell]!=NULL)
				{
					delete(cells[cell]);
					cells[cell] = NULL;
				}
			}
		}

		void dmp(arr* in)
		{
			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			int i = getBufferOrNumber(ar1);
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			int n = getBufferOrNumber(ar2);
			for(; i<=n; ++i)
			{
				cout << i << ": ";
				if(cells[i]==NULL)
					cout << "null\n";
				else
					cout << cells[i]->getData() << "\n";
			}
		}



		void put(arr* in)
		{
			string temp = in->getNext();
			string cont;
			int level=0;
			if(temp=="{{{")
			{
				level = 1;
				temp = in->getNext();
				if(temp=="}}}")
				{
					return;
				}
			}
			else
			{
				cerr << "Expected '{{{' at word " << exind << " (" << exword << ")\n";
				return;
			}
			while(temp!=" ")
			{
				cont += temp;
				temp = " " + in->getNext();
				if(temp==" {{{")
					level++;
				if(temp==" }}}")
				{
					if(level<=1)
					{
						break;
					}
					else
					{
						level--;
					}
				}
			}

			int cell = getBuffer(in->getNext());
			if(cells[cell]!=NULL)
				cells[cell]->acceptPut(cont);
			else
			{
				cells[cell] = new MemoryCell;
				cells[cell]->acceptPut(cont);
			}
		}

		void mpt(arr* in)
		{

			string ar1 = in->getNext();
			if(!checkArg(ar1,in))
				return;
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;

			int cell = getBuffer(ar1);
			int outcell = getBuffer(ar2);
			if(cells[outcell]==NULL)
				cells[outcell] = new MemoryCell;
			if(cells[cell]==NULL)
				cells[cell] = new MemoryCell;
			string con = cells[cell]->returnPut();
			cells[outcell]->acceptPut(con);
		}

		void exf(arr* in)
		{
			string funcname = in->getNext();
			for(int i=0; i<functions.size(); i++)
			{
				if(funcname == functions[i]->name)
				{
					mStack* st = new mStack;
					for(int j=0; j<functions[i]->argcount; j++)
					{
						string tem = in->getNext();
						if(checkArg(tem,in))
						{
							st->arra.push_back(getBufferOrNumber(tem));
						}
					}
					string b = in->getNext();
					if(b!="nul")
					{
						if(checkArg(b,in))
							setBuffer(getBuffer(b), functions[i]->func(st));
					}
					else
					{
						functions[i]->func(st);
					}
					return;
				}
			}
			cerr << "Invalid external function at word " << exind << " (" << exword << ")\n";
		}

		void exc(arr* in)
		{
			string code;
			int cell = getBuffer(in->getNext());
			if(cells[cell]!=NULL)
				code = cells[cell]->returnPut();
			else
			{
				cerr << "Empty cell at word " << exind << " (" << exword << ")\n";
				return;
			}
			mState* newstate = new mState;
			for(int i=0; i<functions.size(); i++)
			{
				mFunc* il = functions[i];
				newstate->addFunction(il->name,il->argcount,il->func);
			}
			for(int i=0; i<buffers.size(); i++)
			{
				mBuffer* il = buffers[i];
				newstate->addBuffer(il->name,il->func);
			}

			if(globalBuffer==NULL)
				globalBuffer = new BufCell;
			newstate->executeCode(code, globalBuffer);
		}

		void als(arr* in){
			string ar1 = in->getNext();
			string ar2 = in->getNext();
			if(!checkArg(ar2,in))
				return;
			aliases.push_back(alias(ar1,ar2));
		}

		void prc(arr* in){
			string ar1 = in->getNext();
			string codeOut = checkInProcedures(ar1);
			if(codeOut != "")
				executeCode(codeOut);
			else
				cerr << "Procedure \'" << ar1 << "\' couldn't be found\n";
		}

		BufCell* globalBuffer;

		struct cmdd
		{
			string name;
			int argcount;
			cmdd(string nam, int args)
			{
				name = nam;
				argcount = args;
			}
		};

		vector<cmdd*> cmds;

		int isInList(string name, vector<cmdd*> lil)
		{
			for(int i=0; i<lil.size(); i++)
			{
				if(name==lil[i]->name)
				{
					return i;
				}
			}
			return -1;
		}

		bool syntaxCheck(arr* in)
		{
			cmds.push_back(new cmdd("buf",2));
			cmds.push_back(new cmdd("mov",2));
			cmds.push_back(new cmdd("lbl",1));
			cmds.push_back(new cmdd("jmp",1));
			cmds.push_back(new cmdd("jgt",3));
			cmds.push_back(new cmdd("jlt",3));
			cmds.push_back(new cmdd("jeq",3));
			cmds.push_back(new cmdd("jnq",3));
			cmds.push_back(new cmdd("add",2));
			cmds.push_back(new cmdd("sub",2));
			cmds.push_back(new cmdd("mul",2));
			cmds.push_back(new cmdd("div",2));
			cmds.push_back(new cmdd("del",1));
			cmds.push_back(new cmdd("dmp",2));
			cmds.push_back(new cmdd("bjp",0));
			cmds.push_back(new cmdd("exc",1));
			cmds.push_back(new cmdd("put",0));
			cmds.push_back(new cmdd("mpt",2));
			cmds.push_back(new cmdd("als",2));
			cmds.push_back(new cmdd("prc",1));

			vector<int> levels;
			int level=0;
			int argg = 0;
			string temp = in->getNext();
			while(temp!="")
			{
				if(temp=="{{{")
				{
					levels.push_back(in->index);
					level++;
				}
				if(temp=="}}}")
				{
					level--;
					levels.erase(levels.begin()+level);
					if(level<=0)
						argg=2;
				}

				if(argg==0)
				{
					int i = isInList(temp,cmds);
					if(i!=-1)
					{
						argg = cmds[i]->argcount;
					}
					else
					{
						if(level<=0)
						{
							if(temp=="exf")
							{
								bool found = false;
								temp = in->getNext();
								for(int g = 0; g<functions.size(); g++)
								{
									if(temp==functions[g]->name)
									{
										argg = functions[g]->argcount+1;
										found = true;
									}
								}
								if(found==false)
								{
									cerr << "Unknown external function at " << in->index << " (" << in->arra[in->index-1] << ")" << "\n";
									return false;
								}
							}
							else
							{
								cerr << "Unknown command at " << in->index << " (" << in->arra[in->index-1] << ")" << "\n";
								return false;
							}
						}
					}
				}
				else
				{
					if(level<=0)
					{
						if(isInList(temp,cmds)==-1)
							argg--;
						else
						{
							cerr << "Expected argument, found command at " << in->index << " (" << in->arra[in->index-1] << ")" << "\n";
							return false;
						}
					}
				}
				temp = in->getNext();
			}
			if(level==0)
			{
				if(argg==0)
				{
					in->index = 0;
					return true;
				}
				else
				{
					cerr << "Expected argument, found nothing at " << in->index+1 << " (after " << in->arra[in->index-1] << ")" << "\n";
					return false;
				}
			}
			else
			{
				cerr << "Forgot to close {{{ at " << levels[max((int)levels.size()-1,0)] << "\n";
				return false;
			}
		}

	public:


		void executeCode(string code, BufCell* buff)
		{
			globalBuffer = buff;
			executeCode(code);
		}

		void executeCode(string code)
		{
			stringstream a;
			a << code;
			arr ll;
			int i = 0;
			bool labelNext = 0;
			bool comment = 0;

			procedure* currentProc = 0;
			bool nameIsSet = false;
			int procedureDepth = 0;

			while(a.good())
			{
				string b;
				a >> b;

				if(labelNext==1&&!comment)
				{
					labelNext = 0;
					lbl c;
					c.index = i+1;
					c.label = b;
					labels.push_back(c);
				}

				if(b=="def"){
					if(currentProc == 0){
						nameIsSet = false;
						currentProc = new procedure();
						procedureDepth = 0;
					} else
						procedureDepth++;
				}

				if(currentProc != 0){
					if(!nameIsSet){
						if(procedureDepth==0)
							procedureDepth++;
						else
						{
							currentProc->name = b;
							nameIsSet = true;
						}
					} else {
						if(b=="fed"){
							if(procedureDepth<=1){
								procedures.push_back(*currentProc);
								delete currentProc;
								currentProc = 0;
							} else {
								currentProc->code += b + " ";
								procedureDepth--;
							}
						} else
							currentProc->code += b + " ";
					}
				} else {

					if(b=="lbl")
						labelNext = 1;
					if(b=="***")
						comment = !comment;
					if(!comment&&b!="***")
					{
						ll.arra.push_back(b);
						++i;
					}

				}


			}
			// checking code

			if(!syntaxCheck(&ll))
				return;

			// reading code
			while(ll.index<ll.arra.size())
			{
				string cur = ll.getNext();
				exind = ll.index;
				exword = cur;
				switch(h(cur.c_str()))
				{
				case h("mov"):
					mov(&ll);
					break;
				case h("buf"):
					buf(&ll);
					break;
				case h("lbl"):
					ll.getNext();
					break;
				case h("jmp"):
					jmp(&ll);
					break;
				case h("jgt"):
					jgt(&ll);
					break;
				case h("jlt"):
					jlt(&ll);
					break;
				case h("jeq"):
					jeq(&ll);
					break;
				case h("jnq"):
					jnq(&ll);
					break;
				case h("add"):
					add(&ll);
					break;
				case h("sub"):
					sub(&ll);
					break;
				case h("mul"):
					mul(&ll);
					break;
				case h("div"):
					div(&ll);
					break;
				case h("del"):
					del(&ll);
					break;
				case h("dmp"):
					dmp(&ll);
					break;
				case h("bjp"):
					bjp(&ll);
					break;
				case h("put"):
					put(&ll);
					break;
				case h("exf"):
					exf(&ll);
					break;
				case h("exc"):
					exc(&ll);
					break;
				case h("mpt"):
					mpt(&ll);
					break;
				case h("als"):
					als(&ll);
					break;
				case h("prc"):
					prc(&ll);
					break;
				}
			}
			for(int i=0; i<10000000; i++)
				if(cells[i]!=NULL)
					delete(cells[i]);
		}

		void executeFile(string filename)
		{
			ifstream t(filename);

			if(!t)
			{
				cerr << "File not found or unable to open";
				return;
			}

			string str;

			t.seekg(0,ios::end);
			str.reserve(t.tellg());
			t.seekg(0,ios::beg);
			str.assign((istreambuf_iterator<char>(t)),
					   istreambuf_iterator<char>());

			try
			{
				executeCode(str);
			}
			catch(exception &e)
			{
				cerr << e.what() << "\n\nError happened on word " << exind << " (" << exword << ")\n";
			}
		}

		void addBuffer(string name, MemoryCell*(* func)())
		{
			mBuffer* a = new mBuffer;
			a->name = name;
			a->func = func;
			buffers.push_back(a);
		}

		void addFunction(string name, int argcount, int(* func)(mStack*))
		{
			mFunc* a = new mFunc;
			a->name = name;
			a->argcount = argcount;
			a->func = func;
			functions.push_back(a);
		}

		MemoryCell* getCellPointer(int cell)
		{
			if(cells[cell]==NULL)
				cells[cell] = new MemoryCell;
			return cells[cell];
		}

		void setCellPointer(MemoryCell* obj, int cell)
		{
			cells[cell] = obj;
		}

	};

}

#endif // MINAAC_H_INCLUDED
