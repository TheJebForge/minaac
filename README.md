# minaac

Minaac (Minaac Is Not An Assembler Clone) is a run-time interpreted programming language capable of recreating Turing Machine making it a turing-complete language.
<br>The language is similar to assembler by syntax, but it's not a clone. You'll understand why when you'll try it.
<br>
It requires C++11 to run.<br><br>
# Installation

To build interpreter you run this command after copying repository into a folder<br>
`g++ -std=c++11 minaac.cpp -o minaac`<br>
<br>Then just run the output file with<br>
`./minaac`

# Documentation
Code is a list of commands and arguments, tabulation or spaces or newlines don't matter for the language while you can still use them for convenience<br><br>
Comments can be specified by using '****' before and after the commented message<br><br>
The language has memory cells as means of doing variables<br>
Hex values like 0x#### are most of the time seen as addresses to cells<br>
0x####i will be seen as hex value instead of an address to a cell<br>
0x####\* will use contents of the cell as a cell, so if 0x#### have for example, 3, then cell 3 will be used in command then<br>
<br>
**Function descriptions:**<br><br>
**\<cell\>** in command arguments means it will see 0x#### as a hex value instead of address despite rules above<br>
**\<value/cell\>** in command arguments means it will use contents of cell 0x#### as input unlike **\<cell\>** arguments<br>
All values are integers<br>
<br>
* buf \<cell\> \<type\> - sets cell \<cell\> as buffer of specified type. Is used to output to screen, input text from user, have buffers, read and write files, and use any other custom buffers.<br>
Built-in buffer types:<br>
> **in** - Input buffer, expects 7 (bell ascii key) to start reading user input and 25 (cancel ascii key) to clear buffer, outputs result character by character from beginning to end.<br>
> **out** - Output buffer, expects any ascii key codes to fill buffer, receives some other ascii keys for controlling the buffer like 13 to output buffer to screen, 2 to set cursor to beginning, 3 to set cursor to end, 21 to move cursor left, 22 to move cursor right, 8 to backspace, 25 to clear buffer, 10 for new line<br>
> **file** - File buffer, expects a filename and then a mode (1 - read, 2 - write, 3 - append), works same as output buffer, but also has 27 to close buffer<br>
> **buffer** - Text buffer, works same as input buffer and output buffer together, doesn't have bell command like input buffer does, and doesn't have 13 for outputing to screen<br>
> **gbuffer** - Global text buffer, works same as text buffer, but is shared between child executions for data transfer (see exc)<br>
> **cell** - Memory cell, all cell are memory cells by default
* mov \<value/cell\> \<cell\> - copies value from cell to cell, or sets value of cell
* lbl \<any word\> - declares a jump point with name \<any word\>, is later called label in docs
* jmp \<label\> - jumps to a jump point no matter if it was declared before or after the command
* jgt \<value/cell\> \<value/cell\> \<label\> - compares first two arguments, and if a is greater than b, jumps to a jump point specified in 3rd argument
* jlt \<value/cell\> \<value/cell\> \<label\> - compares first two arguments, and if a is lesser than b, jumps to a jump point specified in 3rd argument
* jeq \<value/cell\> \<value/cell\> \<label\> - compares first two arguments, and if a equals b, jumps to a jump point specified in 3rd argument
* jnq \<value/cell\> \<value/cell\> \<label\> - compares first two arguments, and if a not equals b, jumps to a jump point specified in 3rd argument
* add \<value/cell\> \<cell\> - similar to mov, but adds instead of set
* sub \<value/cell\> \<cell\> - similar to mov, but subtracts instead of set
* mul \<value/cell\> \<cell\> - similar to mov, but multiplies instead of set
* div \<value/cell\> \<cell\> - similar to mov, but divides instead of set
* del \<cell\> - deletes a cell, a way to remove buffer and have a normal cell instead, or a way to destroy value of the cell
* dmp \<value/cell\> \<value/cell\> - dumps values of cells in range between first argument and second argument including
* bjp - jumps to next command after last jump happened
* exc \<cell\> - executes code from a buffer
* exf \<external function name\> \<any amount of arguments the function requires\> \<return cell/nul\> - Is used to execute external functions, nul can be used instead of return cell address for not setting function out anywhere
* put {{{ \<anything\> }}} \<cell\> - puts text into any buffer, even normal cells
* mpt \<cell\> \<cell\> - copies contents of any buffer into any buffer
* als \<anyWord\> \<value/cell\> - sets an alias for cell/value
* def \<anyWord\> - opens a procedure bracket
* fed - closes procedure bracket
* prc \<anyWord\> - executes a procedure

# Examples of code

See folder "examples" in repository, contains examples of minaac and explanation of C++ API for the language

Useful code to load files in examples folder<br>
`buf 0x1 file
put {{{ <filename> }}} 0x1
mov 1 0x1
exc 0x1`
